// config/database.js
module.exports = {
    databaseConnection:function(mongoose){
        //var url= "mongodb://"+process.env.MONGO_DB_USER+":"+process.env.MONGO_DB_PASS+"@"+process.env.MONGO_DB_HOST+":27017/"+process.env.MONGO_DB_DATABASE;
        try{
            var url = process.env.MONGO_CONNECTION;
            // configuration 
            mongoose.connect(url,{ useNewUrlParser: true });// connect to our database
            console.log(mongoose.connection.readyState);

            // CONNECTION EVENTS
            mongoose.connection.on('connecting', function () {
              console.log('Mongoose default connection open to ' +url);
            });
          
            // When successfully connected
            mongoose.connection.on('connected', function () {
              console.log('MongoDB Connected!!');
              console.log('Mongoose default connected open to ' + url);
            });

            // If the connection throws an error
            mongoose.connection.on('error',function (err) {
              console.log('Mongoose default connection error: ' + err);
            });

            // When the connection is disconnected
            mongoose.connection.on('disconnected', function () {
              console.log('Mongoose default connection disconnected');
            });
        }catch(e){
            console.log('error while connecting DB '+e);
        }
    }
};