// Import Modules
const { getResellerModel } = require("../Models/register-reseller");

// Dashboard: registered tenant count
exports.index = async function(req, res) {

    let resellerModel = await getResellerModel();
    
    let tenant_reg = await resellerModel.count({});
    let tenant_dbs = await resellerModel.where({is_database_created: true}).count();

    let data = {
        tenant_count: tenant_reg,
        tenant_db_count: tenant_dbs
    }

    if(!data)
        res.sendStatus(404)

    res.send(JSON.stringify(data));
}