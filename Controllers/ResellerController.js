const { getResellerModel } = require("../Models/register-reseller");

exports.addReseller = async (req, res) => {

    let db_name = req.body.database_name + "-tenant";
    let tenantModel = await getResellerModel();

    const tenant = new tenantModel({
        contact_person: req.body.contact_person,
        company_name: req.body.company_name,
        company_email: req.body.company_email,
        database_name: db_name,
        is_database_created: false,
    });

    let doc = await tenantModel.findOneAndUpdate({database_name: db_name}, 
    {
        contact_person: req.body.contact_person,
        company_name: req.body.company_name,
        company_email: req.body.company_email,
        database_name: db_name,
        is_database_created: false,
    });
    if (!doc) {
        tenant.save(function (err) {
            // if (err) return handleError(err);
            // saved!
        });
    }

    res.send(JSON.stringify(tenant))
}