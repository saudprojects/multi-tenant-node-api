const { getResellerModel } = require("../Models/register-reseller");
const { getTenantModel } = require("../Models/create-tenant");
const mongoose = require('mongoose');

/**
 * Get tenant list
 * 
 * @param Request req
 * @author SAUD AHMAD
 * @return String JSON
 */ 
exports.getTenantList = async (req, res) => {

    let resellerModel = await getResellerModel();

    let tenant = await resellerModel.find();

    if (!tenant)
        res.sendStatus(404) // tenant not found. Register tenant
    
    res.send(JSON.stringify(tenant))
}

/**
 * Create Tenant DB
 * 
 * @param Request req
 * @author SAUD AHMAD
 * @return String JSON
 */ 
exports.createTenantDB = async (req, res) => {

    let db_name = req.body.database_name;

    let resellerModel = await getResellerModel();
    let tenantModel = await getTenantModel(db_name);

    const reseller = new tenantModel({ 
        contact_person: req.body.contact_person,
        company_name: req.body.company_name,
        company_email: req.body.company_email,
    });

    let doc = await tenantModel.findOneAndUpdate({ db_name }, 
    { 
        contact_person: req.body.database_name,
        company_name: req.body.company_name,
        company_email: req.body.company_email,
    });

    if (!doc) {
        reseller.save(function (err) {
            // if (err) return handleError(err);
            // saved!
        });
    }

    let tenant_doc = await resellerModel.findOneAndUpdate({ database_name: db_name },
    { is_database_created: true });

    res.sendStatus(200)
}

/**
 * Delete tenant
 * 
 * @param Request req
 * @author SAUD AHMAD
 * @return String JSON
 */ 
exports.deleteTenant = async (req, res) => {

    let id = req.body._id;

    let resellerModel = await getResellerModel();

    let tenant = await resellerModel.findOneAndDelete({ _id: id });

    if(!tenant)
        res.sendStatus(404)

    res.sendStatus(200);
}

/**
 * Delete tenant database
 * 
 * @param Request req
 * @author SAUD AHMAD
 * @return Response res
 */ 
exports.deleteTenantDB = async (req, res) => {

    let db_name = req.body.database_name;

    let resellerModel = await getResellerModel();
    let tenantModel = await getTenantModel(db_name);

    tenantModel.db.dropDatabase( async function(err) {
        if(err)
            res.sendStatus(404);

        let tenant_doc = await resellerModel.findOneAndUpdate({ database_name: db_name },
            { is_database_created: false });

        if(!tenant_doc)
            res.sendStatus(404);
    });

    res.sendStatus(200);
}