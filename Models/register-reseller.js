const mongoose = require('mongoose');

// Admin Schema
const resellerSchema = new mongoose.Schema({
    contact_person: String,
    company_name: String,
    company_email: String,
    database_name: String,
    is_database_created: Boolean,
}, { timestamps: true })

const getResellerModel = async () => {
    return mongoose.model("registered-tenants", resellerSchema)
}

module.exports = { getResellerModel }