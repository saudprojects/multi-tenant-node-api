const { connect } = require('../Config/tenant-datatbase');
const mongoose = require('mongoose');
let db;

// Customer Schema
const resellerTenantSchema = new mongoose.Schema({
    contact_person: String,
    company_name: String,
    company_email: String,
}, { timestamps: true })

const getTenantDB = async (database_name) => {
    var url = process.env.MONGO_CONNECTION;
    db = db ? db : await connect(url)
    let tenantDb = db.useDb(database_name, { useCache: true });
    return tenantDb;
}

const getTenantModel = async (database_name) => {
    const tenantDb = await getTenantDB(database_name);
    return tenantDb.model("reseller-details", resellerTenantSchema);
}

module.exports = { getTenantModel }