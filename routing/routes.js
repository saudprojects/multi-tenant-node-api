module.exports = (app) => {
    // Import Controllers
    var DashboardController = require('../Controllers/DashboardController');
    var ResellerController = require('../Controllers/ResellerController');
    var TenantController = require('../Controllers/TenantController');

    // Web Routes
    app.get('/', DashboardController.index);
    app.post('/add-reseller', ResellerController.addReseller);
    app.post('/create-tenant-db', TenantController.createTenantDB);
    app.get('/tenant-list', TenantController.getTenantList);
    app.post('/delete-tenant', TenantController.deleteTenant);
    app.post('/delete-tenant-db', TenantController.deleteTenantDB);
}