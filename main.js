var express = require('express');
var mongoose = require('mongoose');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');

// Global variables
global.path = require('path');
global.dotenv = require('dotenv');

// Config .env file
dotenv.config();

// For getting req.body data from url (body-parser)
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false, parameterLimit: 50000 }));

/* 
CORS is shorthand for Cross-Origin Resource Sharing. It is a mechanism to allow or restrict requested resources on a web server depend on where the HTTP request was initiated.
*/
var cors = require('cors')
app.use(cors())

// MongoDB promise
mongoose.Promise = global.Promise;

// MongoDB Connection
var db = require('./Config/database');
db.databaseConnection(mongoose);

// //add sequelize for orm query and make connection 
// const Sequelize = require('sequelize');
// global.session = require('express-session');
// var MySQLStore = require('express-mysql-session')(session);

// //create connection with node sequelize here 
// global.sequelize = new Sequelize('mysql://'+process.env.DB_USER+':'+process.env.DB_PASS+'@'+process.env.DB_HOST+'/'+process.env.DB_DATABASE, {
//   timezone: '+05:30',
//   //timezone: '+01:00',
//   pool: {
//     max: 5,
//     min: 0,
//     idle: 1000,
//   },
//   dialectOptions: {
//     multipleStatements: true
//   }, 
//   logging: false
// });

// global.connection = mysql.createConnection({
//     host: process.env.DB_HOST,
//     user: process.env.DB_USER,
//     password: process.env.DB_PASS,
//     database: process.env.DB_DATABASE,
//     dateStrings: true,
//     multipleStatements: true
// });

// Default Routing 
var apiRoutes = express.Router();
require('./routing/routes')(apiRoutes);
app.use('/api', apiRoutes);

// Listening port
app.use("/public", express.static(path.join(__dirname, 'public')));
const server = app.listen(process.env.PORT || 5001, function () {
    console.log('Listening on http://localhost:' + (process.env.PORT || 5001));
    global.servervalue = server;
});

// //include models here
// global.Models = require('./Models');

